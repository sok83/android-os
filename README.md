## Android OS Instructions
**************************

### Wiki Guide

1. [Android OS Setup Requirements](https://bitbucket.org/sok83/android-os/wiki/Android%20OS%20Setup%20Requirements)

2. [How to Build Android ROMs on Ubuntu 16.04](https://bitbucket.org/sok83/android-os/wiki/How%20to%20Build%20Android%20ROMs%20on%20Ubuntu%2016.04)

3. [Build & Flash Android Custom ROM for Samsung I9500 Galaxy S4](https://bitbucket.org/sok83/android-os/wiki/Build%20&%20Flash%20Android%20Custom%20ROM%20for%20Samsung%20I9500%20Galaxy%20S4)

4. [Build & Flash Android Custom ROM for Xiaomi Redmi Note 2]()

5. [Build & Flash Android Custom ROM for Huawei Nova 2i RNE-L22]()

6. [ADB and FastBoot](https://bitbucket.org/sok83/android-os/wiki/ADB%20and%20FastBoot)

7. [ROMS Github List with Repo Initialization Commands](https://bitbucket.org/sok83/android-os/wiki/ROMS%20Github%20List%20with%20Repo%20Initialization%20Commands)

### Keywords

- `ROM` : [Read Only Memory](https://en.wikipedia.org/wiki/Read-only_memory)

- `AOSP` : [Android Open Source Project](https://en.wikipedia.org/wiki/Android_(operating_system)#AOSP)

- `OHA` : [Open Handset Alliance (led by Google)](https://en.wikipedia.org/wiki/Open_Handset_Alliance)

- `OEM` : [Original Equipment Manufaturer](https://en.wikipedia.org/wiki/Original_equipment_manufacturer)

- `HALs` : [Hardware Abstraction Layers](https://source.android.com/devices/architecture/hal)

- `HIDL` : [HAL Interface Definition Language](https://source.android.com/devices/architecture/hidl)

- `ADB` : [Android Debug Bridge](https://developer.android.com/studio/command-line/adb)

- `OTA Updates` : [over-the-air Updates](https://source.android.com/devices/tech/ota)

- `OTA Package Tools` : [over-the-air Package Tools](https://source.android.com/devices/tech/ota/tools)

- `CCache` : [compiler cache tool](https://ccache.samba.org/)

